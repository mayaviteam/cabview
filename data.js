var dash_tree = [
	{
		title: "Fashion",
		icon: "fashion.png",
		children: [
			{
				title: "Hair Style",
				icon: "hairstyle.png",
				cmp: {
					xtype: "carousel",
					fullscreen: true,
					defaults: {
						styleHtmlContent: true
					},
					items: [
						{
							html: 'Item 1',
							style: 'background-color: #5E99CC'
                        },
						{
							html: 'Item 2',
							style: 'background-color: #759E60'
                        },
						{
							html: 'Item 3'
                        }
                    ]
				}
            }
        ]
    },
	{
		title: "Sudoku",
		icon: "sudoku.png",
    },
	{
		title: "G11",
		icon: "icon4",
    },
	{
		title: "G2",
		icon: "icon4",
    },
	{
		title: "Games",
		icon: "games.png",
		children: [
			{
				title: "Rubik's Cube",
				cmp: {
					xtype: "rubikscube"
				}
            },
			{
				title: "B2",
				xtype: "panel",
            },
			{
				title: "B3",
				children: [
					{
						title: "C1",
						cmp: {
							xtype: "panel",
							html: "Game On"
						}
                    }
                ]
            }
        ]
    }
]
