/**
 * Created by ddeodar on 4/24/2014.
 */
Ext.Loader.setConfig({
	enabled: true
});

Ext.application({
	name: 'CabView',
	views: ['Gender', 'Advertisement','AgeCategory','RotatingCarousel',
			'Container','Dashboard','DashboardCell','AddOn.RubiksCube','Home','RotatingHomeCarousel'],
	controllers: ['Main'],
	requires: ['CabView.util.Config'],
	viewport: {
		layout: {
			type: 'hbox',
			align: 'middle',
			pack: 'center'
		},
		width:'1280px',
		height:'800px',
		style: {
			backgroundImage: 'url(resources/images/bg2.jpg)',
			backgroundSize: '100% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'bottom left'
		}
	},
	launch: function () {
		Ext.Viewport.add([ {
			xtype: 'home_panel'
		}, {
			xtype: 'advertisement_panel'
		}]);
	}
});
