Ext.define('CabView.controller.Main', {
	extend: 'Ext.app.Controller',
	config: {

		control: {
			'image[action=male]': {
				tap: function(){
					this.onImageTap('m');
				}
			},
			'image[action=female]': {
				tap: function(){
					this.onImageTap('f');
				}
			},
			'image[action=category1]': {
				tap: function(){
					this.onCategoryTap('1');
				}
			},
			'image[action=category2]': {
				tap: function(){
					this.onCategoryTap('2');
				}
			},
			'image[action=category3]': {
				tap: function(){
					this.onCategoryTap('3');
				}
			},
			'button[action=home]':{
				tap: function(){
                    var nav = Ext.ComponentQuery.query("container_panel")[0];
                    nav.pop(nav.getItems().length - 1);
				}
			},
            'dashboard_cell':{
                tap: function(data){
                    var nav = Ext.ComponentQuery.query("container_panel")[0];
                    if(data.children){
                        nav.push(Ext.create('CabView.view.Dashboard', {'dash_obj':data.children}));
                    }else if(data.cmp){
                        var obj = data.cmp;
                        nav.push(obj);
                    }

                }
            }
		}
	},

	onImageTap: function (type) {
		CabView.util.Config.setGender(type);
		Ext.ComponentQuery.query("container_panel")[0].push(Ext.create('CabView.view.AgeCategory'));
	},
	onCategoryTap: function (category) {
		Ext.ComponentQuery.query("container_panel")[0].push(Ext.create('CabView.view.Dashboard',{'dash_obj':dash_tree}));
	}
});
