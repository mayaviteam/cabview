/**
Application specific constant file
*/
Ext.define('CabView.util.Config', {
	singleton: true,

	config: {
		// Application panel height
		appPanelHeight: Ext.Viewport.getSize().height+'px',
		// Application panel  width
		appPanelWidth: Ext.Viewport.getSize().width+'px',
		// Height of panel which holds content
		contentPanelHeight: Ext.Viewport.getSize().height +'px',
		// Width of panel which holds content
		contentPanelWidth: (Ext.Viewport.getSize().width * .6)+'px',
		// Height of panel which holds advertisement
		addPanelHeight: Ext.Viewport.getSize().height +'px',
		// Width of panel which holds advertisement
		addPanelWidth: (Ext.Viewport.getSize().width * .4)+'px',
		// used to identify whether user is male/female
		// default is m for male and for female f
		gender : 'm'

	},

	constructor: function (config) {
		this.initConfig(config);

		this.callParent(config);
	}
});
