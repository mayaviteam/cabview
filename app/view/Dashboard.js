Ext.define('CabView.view.Dashboard', {
	extend: 'Ext.Container',
	xtype: 'dashboard_panel',

    build_grid: function(dash){
        var col_count = 3;
        var col = [[],[],[]];
        for (j = 0; j < dash.length; j++) {
			for (k = 0; k < col_count && j < dash.length; k++, j++) {
                var obj = Ext.create('CabView.view.DashboardCell');
                obj.data = dash[j];
                obj.element.on('tap',function(){
                    this.fireEvent('tap',this.data);
                },obj);
                obj.add({
                    xtype: 'image',
                    height: '160px',
                    width: '160px',
                    src: 'resources/images/' + dash[j].icon,
                });
                obj.add({
                        html:'<font style="color:#ffffff">'+ dash[j].title+'</font>',
                });
                col[k].push(obj);
            }
		}
        return col;
    },

    fill_grid: function(col){
        var c1 = this.getComponent(0);
        if(c1){
            c1.add(col[0]);
        }
        var c2 = this.getComponent(1);
        if(c2){
            c2.add(col[1]);
        }
        var c3 = this.getComponent(2);
        if(c3){
            c3.add(col[2]);
        }
    },
    constructor: function (config) {
        this.callParent(config);
        var col = this.build_grid(config.dash_obj);
        this.fill_grid(col);
	},
	config: {
		width: '800px',
		height: '800px',
        layout: {
			type: 'hbox',
			align: 'middle',
			pack: 'center'
		},
		cls: 'semi-trans',
		items: [
			{
				xtype: 'panel',
                layout: 'vbox',
				height: '100%',
				width: '200px',
            },
            {
				xtype: 'panel',
                layout: 'vbox',
				height: '100%',
				width: '200px',
            },
            {
				xtype: 'panel',
                layout: 'vbox',
				height: '100%',
				width: '200px',
            }
		]
	}

});
