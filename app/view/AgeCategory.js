Ext.define('CabView.view.AgeCategory', {
	extend: 'Ext.Container',
	xtype: 'agecategory_panel',
	config: {
		layout: {
			type: 'hbox',
			align: 'middle',
			pack: 'center'
		},
		cls: 'semi-trans'
	},
	resolveImage:function(gender,count){
		if(gender == 'm'){
			return 'resources/images/agecategory/male/m-'+count+'.png';
		}
		else {
			return 'resources/images/agecategory/female/f-'+count+'.png';
		}
	},
	constructor : function (config) {
        this.callParent(config);
		for (j = 1; j <= 3; j++) {
			this.add({
				xtype: 'image',
				width:'200px',
				height:'200px',
				src: this.resolveImage(CabView.util.Config.getGender(),j),
				action:'category'+j
			});
		}
	}
});
