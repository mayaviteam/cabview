homeItems = [];
Ext.define('CabView.view.Home', {
	extend: 'Ext.Container',
	xtype: 'home_panel',
	constructor: function (config) {
		for (j = 1; j <= 5; j++) {
			homeItems.push({
				xtype: 'image',
				src: 'resources/images/home/advertise-'+j+'.jpg'
			});
		}
		this.callParent(arguments);
	},
	config: {
		layout: 'card',
		width: '800px',
		height: '800px',
		cls: 'semi-trans',
		items: [
			{
				xtype: 'rotating_home_carousel',
				width: '800px',
				height: '800px',
				items: homeItems
			},
			{
				xtype: 'container_panel',
				width: '800px',
				height: '800px'
			}
		]
	}

});
