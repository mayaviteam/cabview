Ext.define('CabView.view.AddOn.RubiksCube', {
	extend: 'Ext.Panel',
    xtype: 'rubikscube',
	config: {
        width: CabView.util.Config.getContentPanelWidth(),
        height: CabView.util.Config.getContentPanelHeight(),
		style: 'background-color:black;',
        layout: 'fit',
        html: ['<iframe width=800 height=800 scrolling="no"  frameBorder="0" style="background:#000000;" src="https://gstatic.com/logos/2014/rubiks/iframe/index.html#data=%7B%22dir%22%3A%22%22%2C%22fpdoodle%22%3A%220%22%2C%22hl%22%3A%22en%22%2C%22origin%22%3A%22https%3A%2F%2Fwww.google.co.in%22%2C%22session%22%3A%2214004753836730.15890263160690665%22%7D"></iframe>']
	}
});
