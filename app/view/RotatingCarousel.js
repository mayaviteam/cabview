var me;
var it = [];
Ext.define('CabView.view.RotatingCarousel', {
		extend: 'Ext.carousel.Carousel',
		alternateClassName: 'Ext.RotatingCarousel',
		xtype: 'rotating_carousel',
		showAnimation: 'fadeIn',
		config: {
			delay: 5000,
			start: true,
			listeners: {
				tap: {
					fn: function () {
						this.pause();
						Ext.Msg.prompt('Email','Please enter your email to avail surprises',
							function (btn, text) {
								var response = new Ext.MessageBox().show({
									message: 'An email has been sent to ' + text,
									fn: function (id, value, opt) {
										me.start();
									}
								});
							});
					},
					element: 'element'
				},
				swipe: {
					fn: function () {
						this.start();
					},
					element: 'innerElement'
				}
			}
		},
		initialize: function () {
			me = this;
			it = this.getItems();
			if (this.config.start) {
				this.start();
			}
		},
		rotate: function () {
			if (this.timeout) {
				clearTimeout(this.timeout);
			}
			//console.log('this.getActiveIndex() '+this.getActiveIndex()+' this.getMaxItemIndex() '+this.getMaxItemIndex());
			if (this.getActiveIndex() === this.getMaxItemIndex()) {
				//this.setActiveItem(0, 'slide');
				//this.previous();
				this.activeIndex = -1;
				//this.setOffset(0);
				//this.animationDirection = -1;
				//this.setOffsetAnimated(-this.itemLength);

			} else {
				//console.log('calling next'+it.length);
				//this.next();
			}
			this.next();
			this.timeout = Ext.defer(this.rotate, this.config.delay, this);
		},
		start: function (delayStart) {
			this.timeout = Ext.defer(this.rotate, delayStart || this.config.delay, this);
		},
		pause: function (delayStart) {
			if (this.timeout) {
				clearTimeout(this.timeout);
			}
			if (delayStart) {
				this.start(delayStart);
			}
			return this;
		},
		stop: function (delayStart) {
			this.pause(delayStart);
			this.setActiveItem(0, 'slide');
			return this;
		}
	});
