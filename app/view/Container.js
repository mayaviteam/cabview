Ext.define('CabView.view.Container', {
	extend: 'Ext.NavigationView',
	xtype: 'container_panel',


	config: {
		cls: 'semi-trans',
		layout: 'card',
		width: '800px',
		height: '800px',
		navigationBar: {
            style: 'background:transparent;',
			items: [
				{
					xtype: 'button',
					text: 'Home',
					align: 'right',
					action:'home'
            }
        ]
		},
		items: [

			{
				xtype: 'gender_panel',
				widht: '800px',
				height: '800px',
			}
		]
	}

});
