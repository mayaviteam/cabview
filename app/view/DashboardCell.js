Ext.define('CabView.view.DashboardCell', {
	extend: 'Ext.Panel',
	xtype: 'dashboard_cell',
    config: {
		cls: 'semi-trans',
        border: 0   ,
        style: 'border-color: #DDD; border-style: solid;',
        layout: {
            type: 'vbox',
            align: 'middle',
            pack: 'center'
        },
    }
});
