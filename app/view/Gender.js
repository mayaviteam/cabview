Ext.define('CabView.view.Gender', {
	extend: 'Ext.Container',
	xtype: 'gender_panel',
	config: {
		layout: {
			type: 'hbox',
			align: 'middle',
			pack: 'center'
		},
		cls: 'semi-trans',
		items: [{
				xtype: 'image',
				height: '200px',
				width: '200px',
				src: 'resources/images/male.png',
				action:'male'

		}, {
				// empty panel for spacing
				xtype: 'panel',
				width: '100px'
		},
			{
				xtype: 'image',
				height: '200px',
				width: '200px',
				src: 'resources/images/female.png',
				action:'female'
		}]
	}

});
