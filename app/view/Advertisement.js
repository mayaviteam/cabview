leaflets = [];
Ext.define('CabView.view.Advertisement', {
	extend: 'Ext.Container',
	xtype: 'advertisement_panel',
	constructor: function (config) {
		for (j = 1; j <= 8; j++) {
			leaflets.push({
				xtype: 'image',
				src: 'resources/photos/ads/' + j + '.jpg'
			});
		}
		this.callParent(arguments);
	},
	config: {
		cls: 'semi-trans',
		items: [
			{
				xtype: 'rotating_carousel',
				width: '480px',
				height: '800px',
				items: leaflets
			}
		]
	}
});
